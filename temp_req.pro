#-------------------------------------------------
#
# Project created by QtCreator 2013-11-11T20:41:43
#
#-------------------------------------------------

QT       += core network webkit webkitwidgets

QT       -= gui

TARGET = temp_req
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    web_termometr.cpp

HEADERS += \
    web_termometr.h
