#ifndef WEB_TERMOMETR_H
#define WEB_TERMOMETR_H

#include <QThread>
#include <QTimer>
#include <QWebPage>
#include <QWebFrame>
#include <QWebView>

class web_termometr : public QWidget
{
    Q_OBJECT
public:
    explicit web_termometr(QApplication *a, QWidget *parent = 0);
    QTimer *maint;
    QApplication *app;
    QWebPage *page;
    int temper=-300;

signals:
    void complete(int);
public slots:
    void recive_temp();

};

#endif // WEB_TERMOMETR_H
