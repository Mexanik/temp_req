#include "web_termometr.h"
#include <QDebug>
#include <QApplication>
#include <QXmlStreamReader>

web_termometr::web_termometr(QApplication *a, QWidget *parent)
{
    try
    {
        app = a;
        page = new QWebPage();
        page->settings()->setAttribute(QWebSettings::AutoLoadImages, false);
        QObject::connect(page, SIGNAL(loadFinished(bool)), this, SLOT(recive_temp()));
        page->mainFrame()->load (QUrl ("http://export.yandex.ru/weather-ng/forecasts/27331.xml"));
    }
    catch(...)
    {}
}

void web_termometr::recive_temp()
{
    try
    {
        QString t = page->mainFrame()->toHtml();
        QXmlStreamReader xml(t);
        while (!xml.atEnd() && !xml.hasError())
        {
            QXmlStreamReader::TokenType token = xml.readNext();
            if (token == QXmlStreamReader::StartDocument)
                continue;
            if (token == QXmlStreamReader::StartElement)
            {
                if (xml.name() == "temperature"){
                    xml.readNext();
                    temper = xml.text().toInt();
                    break;
                }

            }
        }
        emit complete(temper);
        app->exit(temper);
    }
    catch(...)
    {}
}
